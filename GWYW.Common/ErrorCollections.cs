﻿using System;
using System.Collections.Generic;

namespace GWYW.Common
{
	public static class ApplicationError
	{
		private static readonly Dictionary<ErrorCode, string> DicErrorMessage = new Dictionary<ErrorCode, string>
		{
				{ ErrorCode.TC0001, "Corporate Email is Invalid"},   //Token Controller Errors
                { ErrorCode.TC0002, "Please enter only corporate Email"},
				{ ErrorCode.TC0003, "Enter Corporate Email"},
				{ ErrorCode.TC0004, "Invalid corporate mail or password"},
				{ ErrorCode.TC0005, "Enter Password"},
				{ ErrorCode.TC0006, "Corporate email doesn't exists"},
				{ ErrorCode.TC0007, "Enter Corporate Email"},
				{ ErrorCode.TC0008, "Invalid grant_type"},
				{ ErrorCode.TC0009, "Invalid Password"},
				{ ErrorCode.TC0010, "Can not add token to database"},
				{ ErrorCode.TC0011, "Can not refresh token"},
				{ ErrorCode.TC0012, "Refresh token has expired"},
				{ ErrorCode.TC0013, "Can not expire token or a new token"},
				{ ErrorCode.TC0014, "Marathon not found"}
		};

		public static string GetErrorMessage(ErrorCode errorCode)
		{
			return DicErrorMessage != null && DicErrorMessage.ContainsKey(errorCode)
				? DicErrorMessage[errorCode]
				: "Please contact Administrator";
		}
	}

	public enum ErrorCode
	{
		TC0001 = 1001,   //TC=TokenController 
		TC0002 = 1002,
		TC0003 = 1003,
		TC0004 = 1004,
		TC0005 = 1005,
		TC0006 = 1006,
		TC0007 = 1007,
		TC0008 = 1008,
		TC0009 = 1009,
		TC0010 = 1010,
		TC0011 = 1011,
		TC0012 = 1012,
		TC0013 = 1013,
		TC0014 = 1014
	}
}
