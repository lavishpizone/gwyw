﻿using GWYW.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GWYW.Service.Interface
{
	public interface IShopService
	{
		Task<IPagedList<GWYWshopsModel>> GetAllShopsDetails(int pageSize = 10, int pageIndex = 0);
		Task<GWYWshopsModel> GetShopDetail(int? id);
		Task<string> CreateNewShop(GWYWshopsModel model);
		Task<string> UpdateShopProfile(GWYWshopsModel model);
		Task<string> DeleteShopProfile(int id);
	}
}
