﻿using GWYW.Entity;
using GWYW.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GWYW.Service.Interface
{
	public interface IUserService
	{
		bool IsUserEmailExists(string email);
		GWYWmembers IsUserExists(string email, string password);
		Task<string> Setting(GeneralSettingModel generalSetting);
	}
}
