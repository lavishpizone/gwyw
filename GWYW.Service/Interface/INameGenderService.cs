﻿using GWYW.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GWYW.Service.Interface
{
	public interface INameGenderService
	{
		Task<IPagedList<GWYWnamegenderModel>> GetAllNameGenderDetails(int pageSize = 10, int pageIndex = 0);
		Task<GWYWnamegenderModel> GetNamegenderDetail(int? id);
		Task<string> CreateNewNameGender(GWYWnamegenderModel model);
		Task<string> UpdateNameGenderProfile(GWYWnamegenderModel model);
		Task<string> DeleteNameGenderProfile(int id);
	}
}
