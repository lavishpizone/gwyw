﻿using GWYW.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace GWYW.Service.Interface
{
	public interface ITokenService
	{
		bool AddToken(Rtoken token);

		bool ExpireToken(Rtoken token);

		Rtoken GetToken(string refresh_token);
	}
}
