﻿using GWYW.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GWYW.Service.Interface
{
	public interface IMemberService
	{
		Task<IPagedList<GWYWmembersModel>> GetAllMembersDetails(int pageSize, int pageIndex);
		Task<GWYWmembersModel> GetMember(int? id);
		Task<string> CreateMember( GWYWmembersModel membersModel);
		Task<string> UpdateProfile(GWYWmembersModel model);
		Task<string> DeleteProfile(int id);
		Task<string> PasswordForgot(string Email);
		Task<string> PasswordReset(ResetpasswordModel resetpasswordModel);
		Task<IPagedList<GWYWmemberfavesModel>> GetAllMembersFavesDetails(int pageSize = 10, int pageIndex = 0);
		Task<GWYWmemberfavesModel> GetMemberFaves(int? id);
		Task<string> CreateMemberFave(GWYWmemberfavesModel model);
		Task<string> UpdateMemberFavesProfile(GWYWmemberfavesModel model);
		Task<string> DeleteMemberFavesProfile(int id);
	}
}
