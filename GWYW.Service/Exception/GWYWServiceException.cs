﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GWYW.Service.Exception
{
	public class GWYWServiceException : System.Exception
	{
		public GWYWServiceException()
		{ }

		public GWYWServiceException(string message)
			: base(message)
		{ }

		public GWYWServiceException(string message, System.Exception innerException)
			: base(message, innerException)
		{ }
	}
}
