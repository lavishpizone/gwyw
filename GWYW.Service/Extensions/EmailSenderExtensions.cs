﻿using GWYW.Service.Interface;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;

namespace GWYW.Service.Extensions
{
	public static class EmailSenderExtensions
	{
		public static Task SendEmailConfirmationAsync(this IEmailSenderService emailSenderService, string email, string link)
		{
			
			if (!string.IsNullOrEmpty(link))
			{
				return emailSenderService.SendEmailAsync(email, "Password",
					$"Please click this link: " +
					$"'{HtmlEncoder.Default.Encode(link)}'");
			}
			else
			{
				return emailSenderService.SendEmailAsync(email, "Registration Successful",
					$"Thank you for Registration ");
			}
			
		}


	}
}
