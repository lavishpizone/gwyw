﻿using GWYW.Entity;
using GWYW.Model;
using GWYW.Service.Exception;
using GWYW.Service.Interface;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GWYW.Service.Service
{
	public class NameGenderService : INameGenderService
	{
		private readonly IUnitOfWork _unitOfWork;
		private readonly GWYWcontext _context;
		private readonly IRepository<GWYWnamegender> _namegenderRepo;
		private readonly ILogger<NameGenderService> _logger;
		private readonly IEmailSenderService _emailSenderService;

		public NameGenderService(GWYWcontext context, IUnitOfWork unitOfWork, ILogger<NameGenderService> logger, IEmailSenderService emailSenderService)
		{
			_context = context;
			_logger = logger;
			_unitOfWork = unitOfWork;
			_namegenderRepo = _unitOfWork.GetRepository<GWYWnamegender>();
			_emailSenderService = emailSenderService;
		}

		public async Task<string> CreateNewNameGender(GWYWnamegenderModel model)
		{
			_logger.LogDebug("Insert New NameGender Details");
			if (string.IsNullOrEmpty(model.NameValue))
				throw new GWYWServiceException("Name value can not be null!");

			var namegender = new GWYWnamegender()
			{
				NameValue = model.NameValue,
				Gender = model.Gender
			};

			await _context.GWYWnamegender.AddAsync(namegender);
			await _context.SaveChangesAsync();
			return "Record Saved Successfully.";
		}

		public async Task<string> DeleteNameGenderProfile(int id)
		{
			_logger.LogDebug("Delete NameGender Details");
			var nameIdExists = await _namegenderRepo.GetFirstOrDefaultAsync(predicate: x => x.NameID == id);

			if (nameIdExists != null)
			{
				_namegenderRepo.Delete(nameIdExists.NameID);
				await _context.SaveChangesAsync();
				return "Record Deleted Successfully.";
			}
			else
				throw new GWYWServiceException("Record not found.");
		}

		public async Task<IPagedList<GWYWnamegenderModel>> GetAllNameGenderDetails(int pageSize = 10, int pageIndex = 0)
		{
			_logger.LogDebug("Get All NameGender Details");
			return await _namegenderRepo.GetPagedListAsync(s => new GWYWnamegenderModel()
			{
				NameID = s.NameID,
				NameValue = s.NameValue,
				Gender = s.Gender
			}, pageSize: pageSize, pageIndex: pageIndex, orderBy: o => o.OrderByDescending(d => d.NameID));
		}

		public async Task<GWYWnamegenderModel> GetNamegenderDetail(int? id)
		{
			_logger.LogDebug("Get NameGender Details");
			return await _namegenderRepo.GetFirstOrDefaultAsync(s => new GWYWnamegenderModel()
			{
				NameID = s.NameID,
				NameValue = s.NameValue,
				Gender = s.Gender
			}, predicate: x => x.NameID == id);
		}

		public async Task<string> UpdateNameGenderProfile(GWYWnamegenderModel model)
		{
			_logger.LogDebug("UPdate NameGender Details");
			var nameIdExists = await _namegenderRepo.GetFirstOrDefaultAsync(predicate: x => x.NameID == model.NameID);

			if (nameIdExists != null)
			{
				nameIdExists.NameValue = model.NameValue;
				nameIdExists.Gender = model.Gender;
				
				_namegenderRepo.Update(nameIdExists);
				await _context.SaveChangesAsync();

				return "Record Update Successfully.";
			}
			else
				throw new GWYWServiceException("Record not found.");
		}
	}
}
