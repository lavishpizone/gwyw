﻿using GWYW.Entity;
using GWYW.Model;
using GWYW.Service.Exception;
using GWYW.Service.Interface;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GWYW.Service.Service
{
	public class ShopService : IShopService
	{
		private readonly IUnitOfWork _unitOfWork;
		private readonly GWYWcontext _context;
		private readonly IRepository<GWYWmembers> _memberRepo;
		private readonly IRepository<GWYWmemberfaves> _memberfavesRepo;
		private readonly IRepository<GWYWshops> _shopRepo;
		private readonly ILogger<ShopService> _logger;
		private readonly IEmailSenderService _emailSenderService;

		public ShopService(GWYWcontext context, IUnitOfWork unitOfWork, ILogger<ShopService> logger, IEmailSenderService emailSenderService)
		{
			_context = context;
			_logger = logger;
			_unitOfWork = unitOfWork;
			_memberRepo = _unitOfWork.GetRepository<GWYWmembers>();
			_memberfavesRepo = _unitOfWork.GetRepository<GWYWmemberfaves>();
			_shopRepo = _unitOfWork.GetRepository<GWYWshops>();
			_emailSenderService = emailSenderService;
		}

		public async Task<string> CreateNewShop(GWYWshopsModel model)
		{
			_logger.LogDebug("Insert Shop Details");
			if (string.IsNullOrEmpty(model.ShopName))
				throw new GWYWServiceException("Shop Name can not be null!");

			if (string.IsNullOrEmpty(model.ShopType))
				throw new GWYWServiceException("Shop Type can not be null!");

			var shop = new GWYWshops()
			{
				ShopID = model.ShopID,
				ShopName = model.ShopName,
				ShopType = model.ShopType,
				BMorONLINE = model.BMorONLINE
			};

			await _context.AddAsync(shop);
			await _context.SaveChangesAsync();
			return "Record Saved Successfully.";
		}

		public async Task<string> DeleteShopProfile(int id)
		{
			_logger.LogDebug("Delete Shop Details");
			var shopIdExists = await _shopRepo.GetFirstOrDefaultAsync(predicate: x => x.ShopID == id);

			if (shopIdExists != null)
			{
				_shopRepo.Delete(shopIdExists.ShopID);
				await _context.SaveChangesAsync();
				return "Record Deleted Successfully.";
			}
			else
				throw new GWYWServiceException("Record not found.");
		}

		public async Task<IPagedList<GWYWshopsModel>> GetAllShopsDetails(int pageSize = 10, int pageIndex = 0)
		{
			_logger.LogDebug("Get All Shops Details");
			return await _shopRepo.GetPagedListAsync(s => new GWYWshopsModel()
			{
				ShopID = s.ShopID,
				ShopName = s.ShopName,
				ShopType = s.ShopType,
				BMorONLINE = s.BMorONLINE
			}, pageSize: pageSize, pageIndex: pageIndex, orderBy: o => o.OrderByDescending(d => d.ShopID));
		}

		public async Task<GWYWshopsModel> GetShopDetail(int? id)
		{
			_logger.LogDebug("Get Shop Details");
			return await _shopRepo.GetFirstOrDefaultAsync(s => new GWYWshopsModel()
			{
				ShopID = s.ShopID,
				ShopName = s.ShopName,
				ShopType = s.ShopType,
				BMorONLINE = s.BMorONLINE
			}, predicate: x => x.ShopID == id);
		}

		public async Task<string> UpdateShopProfile(GWYWshopsModel model)
		{
			_logger.LogDebug("Update Shop Details");
			var shopIdExists = await _shopRepo.GetFirstOrDefaultAsync(predicate: x => x.ShopID == model.ShopID);

			if (shopIdExists != null)
			{
				shopIdExists.ShopID = model.ShopID;
				shopIdExists.ShopName = model.ShopName;
				shopIdExists.ShopType = model.ShopType;
				shopIdExists.BMorONLINE = model.BMorONLINE;
				_shopRepo.Update(shopIdExists);
				await _context.SaveChangesAsync();

				return "Record Update Successfully.";
			}
			else
				throw new GWYWServiceException("Record not found.");
		}
	}
}
