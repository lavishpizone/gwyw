﻿using GWYW.Service.Interface;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.Server;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace GWYW.Service.Service
{
	public class EmailSenderService : IEmailSenderService
	{
		private readonly IHostingEnvironment _hostingEnvironment;
		private readonly IUnitOfWork _unitOfWork;

		public EmailSenderService(IUnitOfWork unitOfWork, IHostingEnvironment hostingEnvironment)
		{
			_unitOfWork = unitOfWork;
			_hostingEnvironment = hostingEnvironment;
		}
		public string createEmailBody(string userName, string message)
		{
			string body = string.Empty;
			string webRootPath = _hostingEnvironment.WebRootPath;
			var path = Path.Combine(_hostingEnvironment.WebRootPath, "RegistrationSuccessfull.html");
			using (StreamReader reader = new StreamReader(path))
			{
				body = reader.ReadToEnd();
			}
			//body = body.Replace("{UserName}", userName); //replacing the required things 
			body = body.Replace("{message}", message);
			return body;

		}

		public Task SendEmailAsync(string email, string subject, string message)
		{
			MailMessage mail = new MailMessage();
			MailAddress mailAddress = new MailAddress("emergeiq@gmail.com", "EmergeIQ");
			mail.To.Add(email);
			mail.From = mailAddress;
			mail.Subject = subject;
			mail.Body = createEmailBody(email, message);
			mail.IsBodyHtml = true;
			string sendEmailsFrom = "emergeiq@gmail.com";
			string sendEmailsFromPassword = "emergeiq@123";
			NetworkCredential credentials = new NetworkCredential(sendEmailsFrom, sendEmailsFromPassword);

			SmtpClient mailClient = new SmtpClient("smtp.gmail.com", 587);
			mailClient.UseDefaultCredentials = false;
			mailClient.Credentials = credentials;
			mailClient.EnableSsl = true;
			System.Net.ServicePointManager.Expect100Continue = false;
			mailClient.DeliveryMethod = SmtpDeliveryMethod.Network;
			mailClient.Send(mail);

			return Task.CompletedTask;
		}

	}
}
