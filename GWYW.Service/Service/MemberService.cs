﻿using GWYW.Entity;
using GWYW.Model;
using GWYW.Service.Exception;
using GWYW.Service.Extensions;
using GWYW.Service.Interface;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Miyabi.Service.CommonService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GWYW.Service.Service
{
	public class MemberService : IMemberService
	{
		private readonly IUnitOfWork _unitOfWork;
		private readonly GWYWcontext _context;
		private readonly IRepository<GWYWmembers> _memberRepo;
		private readonly IRepository<GWYWmemberfaves> _memberfavesRepo;
		private readonly IRepository<GeneralSetting> _generalSettingRepo;
		private readonly IRepository<GWYWshops> _shopRepo;
		private readonly ILogger<MemberService> _logger;
		private readonly IEmailSenderService _emailSenderService;

		public MemberService(GWYWcontext context, IUnitOfWork unitOfWork, ILogger<MemberService> logger, IEmailSenderService emailSenderService)
		{
			_context = context;
			_logger = logger;
			_unitOfWork = unitOfWork;
			_memberRepo = _unitOfWork.GetRepository<GWYWmembers>();
			_memberfavesRepo = _unitOfWork.GetRepository<GWYWmemberfaves>();
			_generalSettingRepo = _unitOfWork.GetRepository<GeneralSetting>();
			_shopRepo = _unitOfWork.GetRepository<GWYWshops>();
			_emailSenderService = emailSenderService;
		}


		public async Task<IPagedList<GWYWmembersModel>> GetAllMembersDetails(int pageSize, int pageIndex)
		{
			_logger.LogDebug("All Members Details");
			return await _memberRepo.GetPagedListAsync(s => new GWYWmembersModel()
			{
				MemberID = s.MemberID,
				FirstName = s.FirstName,
				LastName = s.LastName,
				DOB = s.DOB,
				Gender = s.Gender,
				StreetAddress1 = s.StreetAddress1,
				StreetAddress2 = s.StreetAddress2,
				City = s.City,
				StateCode = s.StateCode,
				Zip = s.Zip,
				ZipPlus5 = s.ZipPlus5,
				Email = s.Email,
				CellNumber = s.CellNumber,
				//Password = s.Instance.Encrypt(s.Password),
				Role = s.Role,
				CreationDate = s.CreationDate
			}, pageSize: pageSize, pageIndex: pageIndex, orderBy: o => o.OrderByDescending(d => d.CreationDate));
		}

		public async Task<GWYWmembersModel> GetMember(int? id)
		{
			_logger.LogDebug("Member Detail get by Id");
			return await _memberRepo.GetFirstOrDefaultAsync(s => new GWYWmembersModel()
			{
				MemberID = s.MemberID,
				FirstName = s.FirstName,
				LastName = s.LastName,
				DOB = s.DOB,
				Gender = s.Gender,
				StreetAddress1 = s.StreetAddress1,
				StreetAddress2 = s.StreetAddress2,
				City = s.City,
				StateCode = s.StateCode,
				Zip = s.Zip,
				ZipPlus5 = s.ZipPlus5,
				Email = s.Email,
				CellNumber = s.CellNumber,
				//Password = s.Instance.Encrypt(s.Password),
				Role = s.Role,
				CreationDate = s.CreationDate
			}, predicate: x => x.MemberID == id);
		}

		public async Task<string> UpdateProfile(GWYWmembersModel model)
		{
			_logger.LogDebug("Update Member Details");
			var userExists = await _memberRepo.GetFirstOrDefaultAsync(predicate: x => x.MemberID == model.MemberID);

			if (string.IsNullOrEmpty(model.FirstName) || string.IsNullOrEmpty(model.LastName))
				throw new GWYWServiceException("First Name or LastName can not be null or empty");

			if (string.IsNullOrEmpty(model.Email))
				throw new GWYWServiceException("Email can not be null!");

			if (userExists != null)
			{
				userExists.FirstName = model.FirstName;
				userExists.LastName = model.LastName;
				userExists.DOB = model.DOB;
				userExists.Gender = model.Gender;
				userExists.StreetAddress1 = model.StreetAddress1;
				userExists.StreetAddress2 = model.StreetAddress2;
				userExists.City = model.City;
				userExists.StateCode = model.StateCode;
				userExists.Zip = model.Zip;
				userExists.ZipPlus5 = model.ZipPlus5;
				userExists.Email = model.Email;
				userExists.CellNumber = model.CellNumber;
				userExists.Password = EncryptionService.Instance.Encrypt(model.Password);
				userExists.Role = model.Role;
				userExists.CreationDate = model.CreationDate;

				_memberRepo.Update(userExists);

				await _context.SaveChangesAsync();

				return "Profile Update Successfully.";
			}
			else
				throw new GWYWServiceException("User can not found.");
		}

		public async Task<string> DeleteProfile(int id)
		{
			_logger.LogDebug("Delete Member");
			var userExists = await _memberRepo.GetFirstOrDefaultAsync(predicate: x => x.MemberID == id);

			if (userExists != null)
			{
				_memberRepo.Delete(userExists);
				await _context.SaveChangesAsync();
				return "Profile Deleted Successfully.";
			}
			else
				throw new GWYWServiceException("User can not found.");
		}

		public async Task<string> CreateMember(GWYWmembersModel membersModel)
		{
			_logger.LogDebug("Insert New Member");
			if (string.IsNullOrEmpty(membersModel.FirstName))
				throw new GWYWServiceException("FirstName can not be null or empty.");

			if (string.IsNullOrEmpty(membersModel.Email))
				throw new GWYWServiceException("Email can not be null or empty.");

			var emailExists = _memberRepo.GetFirstOrDefault(predicate: x => x.Email == membersModel.Email);

			if (emailExists != null)
				throw new GWYWServiceException("Email Id is already exists.");

			if (membersModel != null)
			{
				GWYWmembers user = new GWYWmembers()
				{
					FirstName = membersModel.FirstName,
					LastName = membersModel.LastName,
					DOB = membersModel.DOB,
					Gender = membersModel.Gender,
					StreetAddress1 = membersModel.StreetAddress1,
					StreetAddress2 = membersModel.StreetAddress2,
					City = membersModel.City,
					StateCode = membersModel.StateCode,
					Zip = membersModel.Zip,
					ZipPlus5 = membersModel.ZipPlus5,
					Email = membersModel.Email,
					CellNumber = membersModel.CellNumber,
					Password = EncryptionService.Instance.Encrypt(membersModel.Password),
					Role = membersModel.Role,
					CreationDate = DateTime.Now
				};

				await _context.AddAsync(user);
				await _context.SaveChangesAsync();

				var userId = await _memberRepo.GetFirstOrDefaultAsync(predicate: x => x.MemberID == membersModel.MemberID);

				var callbackUrl = "";
				await _emailSenderService.SendEmailConfirmationAsync(membersModel.Email, callbackUrl);

				return "Profile Saved Successfully.";
			}
			else
				throw new GWYWServiceException("User not saved.");
		}

		public async Task<string> PasswordReset(ResetpasswordModel resetpasswordModel)
		{
			_logger.LogDebug("Reset Password");
			var userExists = await _memberRepo.GetFirstOrDefaultAsync(predicate: x => x.Email == resetpasswordModel.Email);

			if (EncryptionService.Instance.Encrypt(resetpasswordModel.OldPassword) != userExists.Password)
				throw new GWYWServiceException("Old Password did not match.");

			if ( resetpasswordModel.NewPassword != resetpasswordModel.ConfirmPassword )
				throw new GWYWServiceException("New Password and Confirm password did not match.");

			if (userExists != null)
			{
				userExists.Password = EncryptionService.Instance.Encrypt(resetpasswordModel.NewPassword);
				_memberRepo.Update(userExists);
				await _context.SaveChangesAsync();
				return "Password Updated Successfully.";
			}
			else
				throw new GWYWServiceException("Password not Updated ");
		}

		public async Task<string> PasswordForgot(string Email)
		{
			_logger.LogDebug("Forgot Password");
			var userExists = await _memberRepo.GetFirstOrDefaultAsync(predicate: x => x.Email == Email);

			if (userExists != null)
			{
				//var encryptEmail = EncryptionService.Instance.Encrypt(userExists.Email);
				var data = await _generalSettingRepo.GetFirstOrDefaultAsync(predicate: x => x.Key == "URL");
				var callbackUrl = data.Value; ;
				await _emailSenderService.SendEmailConfirmationAsync(userExists.Email, callbackUrl);
				return "Mail sent Successfully.";
			}
			else
				throw new GWYWServiceException("Password not Updated ");
		}


		public async Task<IPagedList<GWYWmemberfavesModel>> GetAllMembersFavesDetails(int pageSize = 10, int pageIndex = 0)
		{
			_logger.LogDebug("All MemberFaves Details");
			return await _memberfavesRepo.GetPagedListAsync(s => new GWYWmemberfavesModel()
			{
				MemberFaveID = s.MemberFaveID,
				MemberID = s.MemberIDNavigation.MemberID,
				ShopID = s.ShopIDNavigation.ShopID
			}, pageSize: pageSize, pageIndex: pageIndex, orderBy: o => o.OrderByDescending(d => d.MemberFaveID));
		}

		public async Task<GWYWmemberfavesModel> GetMemberFaves(int? id)
		{
			_logger.LogDebug("Get MemberFave Details");
			return await _memberfavesRepo.GetFirstOrDefaultAsync(s => new GWYWmemberfavesModel()
			{
				MemberFaveID = s.MemberFaveID,
				MemberID = s.MemberIDNavigation.MemberID,
				ShopID = s.ShopIDNavigation.ShopID
			}, predicate: x => x.MemberFaveID == id);
		}


		public async Task<string> CreateMemberFave(GWYWmemberfavesModel model)
		{
			_logger.LogDebug("Insert MemberFaves Details");
			var memberIdExists = await _memberRepo.GetFirstOrDefaultAsync(predicate: x => x.MemberID == model.MemberID);
			var shopIdExists = await _shopRepo.GetFirstOrDefaultAsync(predicate: x => x.ShopID == model.ShopID);

			if (memberIdExists == null)
				throw new GWYWServiceException("Member Id not found.");
			if (shopIdExists == null)
				throw new GWYWServiceException("Shop Id not found.");

			var memberfave = new GWYWmemberfaves()
			{
				MemberID = model.MemberID,
				ShopID = model.ShopID
			};

			await _context.AddAsync(memberfave);
			await _context.SaveChangesAsync();

			return "Record Saved Successfully.";
		}


		public async Task<string> UpdateMemberFavesProfile(GWYWmemberfavesModel model)
		{
			_logger.LogDebug("Update Memberfaves Details");
			var userExists = await _memberfavesRepo.GetFirstOrDefaultAsync(predicate: x => x.MemberFaveID == model.MemberFaveID);

			var memberIdExists = await _memberRepo.GetFirstOrDefaultAsync(predicate: x => x.MemberID == model.MemberID);
			var shopIdExists = await _shopRepo.GetFirstOrDefaultAsync(predicate: x => x.ShopID == model.ShopID);

			if (memberIdExists == null)
				throw new GWYWServiceException("Member Id not found.");
			if (shopIdExists == null)
				throw new GWYWServiceException("Shop Id not found.");

			if (userExists != null)
			{
				userExists.MemberID = model.MemberID;
				userExists.ShopID = model.ShopID;
				
				_memberfavesRepo.Update(userExists);
				await _context.SaveChangesAsync();

				return "Record Update Successfully.";
			}
			else
				throw new GWYWServiceException("Record not found.");
		}


		public async Task<string> DeleteMemberFavesProfile(int id)
		{
			_logger.LogDebug("Delete Memberfacves Details");
			var userExists = await _memberfavesRepo.GetFirstOrDefaultAsync(predicate: x => x.MemberFaveID == id);

			if (userExists != null)
			{
				_memberfavesRepo.Delete(userExists.MemberFaveID);
				await _context.SaveChangesAsync();
				return "Record Deleted Successfully.";
			}
			else
				throw new GWYWServiceException("Record not found.");
		}

		
	}
}
