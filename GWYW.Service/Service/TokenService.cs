﻿using GWYW.Entity;
using GWYW.Service.Interface;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace GWYW.Service.Service
{
	public class TokenService : ITokenService
	{
		private readonly IUnitOfWork _unitOfWork;
		private readonly IRepository<Rtoken> _rTokenRepo;
		private ILogger<TokenService> _logger;

		public TokenService(IUnitOfWork unitOfWork, ILogger<TokenService> logger)
		{
			_logger = logger;
			_unitOfWork = unitOfWork;
			_rTokenRepo = _unitOfWork.GetRepository<Rtoken>();
		}

		public bool AddToken(Rtoken token)
		{
			_logger.LogDebug("TokenService->AddToken(): Add refresh token in database.");
			_rTokenRepo.Insert(token);

			return _unitOfWork.SaveChanges() > 0;
		}

		public bool ExpireToken(Rtoken token)
		{
			_logger.LogDebug("TokenService->AddToken(): Update refresh token in database.");
			_rTokenRepo.Update(token);

			return _unitOfWork.SaveChanges() > 0;
		}

		public Rtoken GetToken(string refresh_token)
		{
			_logger.LogDebug("TokenService->AddToken(): Get refresh token in database.");
			return _rTokenRepo.GetFirstOrDefault(predicate: x => x.RefreshToken == refresh_token);
		}
	}
}
