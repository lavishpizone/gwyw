﻿using GWYW.Entity;
using GWYW.Model;
using GWYW.Service.Exception;
using GWYW.Service.Extensions;
using GWYW.Service.Interface;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Miyabi.Service.CommonService;
using System;
using System.Threading.Tasks;

namespace GWYW.Service.Service
{
	public class UserService : IUserService
	{
		private readonly IUnitOfWork _unitOfWork;
		private readonly IRepository<GWYWmembers> _userRepo;
		private readonly IRepository<GeneralSetting> _generalSettingRepo;
		private readonly GWYWcontext _context;
		private readonly IEmailSenderService _emailSenderService;
		private readonly ILogger<UserService> _logger;

		public UserService(IUnitOfWork unitOfWork, GWYWcontext context, IEmailSenderService emailSenderService, ILogger<UserService> logger)
		{
			_logger = logger;
			_context = context;
			_unitOfWork = unitOfWork;
			_userRepo = _unitOfWork.GetRepository<GWYWmembers>();
			_emailSenderService = emailSenderService;
			_generalSettingRepo = _unitOfWork.GetRepository<GeneralSetting>();
		}

		public GWYWmembers IsUserExists(string username, string password)
		{

			return _userRepo.GetFirstOrDefault(predicate: x => x.Email == username && x.Password == EncryptionService.Instance.Encrypt(password));		
		}

		public bool IsUserEmailExists(string username)
		{
			var user = _userRepo.GetFirstOrDefault(predicate: x => x.Email == username);
			return user != null;
		}

		
		public async Task<string> Setting(GeneralSettingModel generalSetting)
		{
			if (string.IsNullOrEmpty(generalSetting.Key))
				throw new GWYWServiceException("Key can not be null or empty.");

			var setting = new GeneralSetting()
			{
				ID = generalSetting.ID,
				Key = generalSetting.Key,
				Value = generalSetting.Value
			};

			_generalSettingRepo.Update(setting);
			await _context.SaveChangesAsync();
			return "General Setting saved Successfully.";
		}


		


	}
}
