﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Miyabi.Service.CommonService
{
	public class EncryptionService : IDisposable
	{
		#region --private data structures--

		private static volatile EncryptionService _instance;
		private static readonly object LockObject = new object();
		private readonly byte[] _key;
		private readonly byte[] _iV;
		private Rfc2898DeriveBytes _cryptoGenerator;

		#endregion

		private EncryptionService()
		{
			string encryptionKey = "8BB8680C41E8"; // TODO: need to get from appsetting.json
			byte[] encodingType = Encoding.ASCII.GetBytes(encryptionKey);
			_cryptoGenerator = new Rfc2898DeriveBytes(encryptionKey, encodingType);
			_key = _cryptoGenerator.GetBytes(32);
			_iV = _cryptoGenerator.GetBytes(16);
		}

		/// <summary>
		/// Get a single service instance for all encryption and decryption services.
		/// </summary>
		public static EncryptionService Instance
		{
			get
			{
				lock (LockObject)
				{
					if (_instance == null)
					{
						_instance = new EncryptionService();
					}
				}
				return _instance;
			}
		}

		/// <summary>
		/// Encrypt the data.
		/// </summary>
		/// <param name="input">The string data to encrypt</param>
		/// <returns>The base64 encrypted string data</returns>
		//public string Encrypt(string input)
		//{
		//    return Convert.ToBase64String(EncryptedBytes(input));
		//}

		/// <summary>
		/// Decrypt the data.
		/// </summary>
		/// <param name="input">The base 64 string data to decrypt.</param>
		/// <returns>A non-encrypted data.</returns>
		//public string Decrypt(string input)
		//{
		//    var encryptedData = Convert.FromBase64String(input);

		//    return Decrypt(encryptedData);
		//}


		/// <summary>
		/// Encrypt the data.
		/// </summary>
		/// <param name="input">The data to encrypt</param>
		/// <returns>An encrypted data</returns>
		public byte[] EncryptedBytes(string input)
		{


			var plainText = Encoding.Unicode.GetBytes(input);

			byte[] encryptedData;

			using (var encryptor = Aes.Create())
			{
				var memoryStream = new MemoryStream();
				var cryptoStream = new CryptoStream(memoryStream, encryptor.CreateEncryptor(), CryptoStreamMode.Write);
				{
					cryptoStream.Write(plainText, 0, plainText.Length);

					cryptoStream.FlushFinalBlock();

					encryptedData = memoryStream.ToArray();
				}
			}

			return encryptedData;
		}

		/// <summary>
		/// Decrypt the data.
		/// </summary>
		/// <param name="input">The base 64 string data to decrypt.</param>
		/// <returns>A non-encrypted data.</returns>
		public string Decrypt(byte[] input)
		{
			string decryptedInput;

			using (var decryptor = Aes.Create().CreateDecryptor(_key, _iV))
			{
				var memoryStream = new MemoryStream(input);
				var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
				var plainText = new byte[input.Length];
				var decryptedCount = cryptoStream.Read(plainText, 0, plainText.Length);
				decryptedInput = Encoding.Unicode.GetString(plainText, 0, decryptedCount);
			}

			return decryptedInput;
		}

		/// <summary>
		/// Decrypt a string guid and return a valid guid.
		/// </summary>
		/// <param name="encryptedGuid">The encrypted guid string.</param>
		/// <returns>va valid GUID</returns>
		public Guid DecryptToGuid(string encryptedGuid)
		{
			var decryptedGuid = "";

			if (!string.IsNullOrEmpty(encryptedGuid))
			{
				decryptedGuid = Decrypt(encryptedGuid);
			}

			//Guid returnGuid;

			return Guid.TryParse(decryptedGuid, out Guid returnGuid) ? new Guid(decryptedGuid) : Guid.Empty;
		}

		/// <summary>
		/// Get 20 length friendly name encrypted GUID.
		/// </summary>
		/// <param name="id"></param>
		/// <returns>20 character encrypted guid string.</returns>
		public string EncryptFriendlyNameGuid(Guid id)
		{
			return Encrypt(id.ToString()).Remove(20);
		}

		~EncryptionService()
		{
			Dispose(false);
		}

		/// <summary>
		/// Dispose of resources.
		/// </summary>
		public void Dispose()
		{
			Dispose(true);

			GC.SuppressFinalize(this);
		}

		protected virtual void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (_cryptoGenerator != null)
				{
					_cryptoGenerator.Dispose();
					_cryptoGenerator = null;
				}
			}
		}
		public string Encrypt(string encryptString)
		{
			string EncryptionKey = "8BB8680C41E8";
			byte[] clearBytes = Encoding.Unicode.GetBytes(encryptString);
			using (Aes encryptor = Aes.Create())
			{
				Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] {
			0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76
		});
				encryptor.Key = pdb.GetBytes(32);
				encryptor.IV = pdb.GetBytes(16);
				using (MemoryStream ms = new MemoryStream())
				{
					using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
					{
						cs.Write(clearBytes, 0, clearBytes.Length);
						cs.Close();
					}
					encryptString = Convert.ToBase64String(ms.ToArray());
				}
			}
			return encryptString;
		}

		public string Decrypt(string cipherText)
		{
			string EncryptionKey = "8BB8680C41E8";
			cipherText = cipherText.Replace(" ", "+");
			byte[] cipherBytes = Convert.FromBase64String(cipherText);
			using (Aes encryptor = Aes.Create())
			{
				Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] {
			0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76
		});
				encryptor.Key = pdb.GetBytes(32);
				encryptor.IV = pdb.GetBytes(16);
				using (MemoryStream ms = new MemoryStream())
				{
					using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
					{
						cs.Write(cipherBytes, 0, cipherBytes.Length);
						cs.Close();
					}
					cipherText = Encoding.Unicode.GetString(ms.ToArray());
				}
			}
			return cipherText;
		}
	}
}
