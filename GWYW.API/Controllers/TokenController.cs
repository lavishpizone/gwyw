﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using GWYW.Common;
using GWYW.Entity;
using GWYW.Model.Common;
using GWYW.Service.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Protocols;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;

namespace GWYW.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TokenController : ControllerBase
    {
		private ITokenService _tokenService;
		private IUserService _userService;

		public TokenController(/*IOptions<Audience> settings,*/ ITokenService tokenService, IUserService userService)
		{
			//_settings = settings;
			_tokenService = tokenService;
			_userService = userService;
		}

		[HttpPost]
		[Route("auth")]
		public async Task Auth([FromBody]Parameters parameters)
		{
			if (parameters.Grant_type == "password")
			{
				if (string.IsNullOrEmpty(parameters.Username))
				{
					Response.StatusCode = 400;
					await Request.HttpContext.Response.WriteAsync(ApplicationError.GetErrorMessage(ErrorCode.TC0003));
				}
				else if (string.IsNullOrEmpty(parameters.Username) && string.IsNullOrEmpty(parameters.Password))
				{
					Response.StatusCode = 400;
					await Request.HttpContext.Response.WriteAsync(ApplicationError.GetErrorMessage(ErrorCode.TC0004));
				}
				else if (!string.IsNullOrEmpty(parameters.Username) && !string.IsNullOrEmpty(parameters.Password))
				{
					await DoPassword(parameters);
				}
				else if (!string.IsNullOrEmpty(parameters.Username) && parameters.Password == "")
				{
					Response.StatusCode = 400;
					await Request.HttpContext.Response.WriteAsync(ApplicationError.GetErrorMessage(ErrorCode.TC0005));

				}
				else if (!string.IsNullOrEmpty(parameters.Username))
				{
					const string emailRegex = @"^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$";
					var re = new Regex(emailRegex);
					if (!re.IsMatch(parameters.Username))
					{

						throw new Exception(ApplicationError.GetErrorMessage(ErrorCode.TC0001));
					}
					re = new Regex(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(?!hotmail|gmail|yahoo)(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
					if (re.IsMatch(parameters.Username))
					{
						var emailExists = _userService.IsUserEmailExists(parameters.Username);
						if (!emailExists)
						{
							Response.StatusCode = 404;
							await Request.HttpContext.Response.WriteAsync(ApplicationError.GetErrorMessage(ErrorCode.TC0006));
						}
						else
						{
							Response.StatusCode = 200;
							var response = new
							{
								Status = 200
							};
							await Request.HttpContext.Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
						}
					}
					else
					{
						throw new Exception(ApplicationError.GetErrorMessage(ErrorCode.TC0002));
					}
				}
			}
			else if (parameters.Grant_type == "refresh_token")
			{
				await DoRefreshToken(parameters);
			}
			else
			{
				Response.StatusCode = 400;
				await Request.HttpContext.Response.WriteAsync(ApplicationError.GetErrorMessage(ErrorCode.TC0008));
			}
		}

		//scenario 1 ï¼š get the access-token by username and password  
		private Task DoPassword(Parameters parameters)
		{
			//validate the client_id/client_secret/username/password  
			var user = _userService.IsUserExists(parameters.Username, parameters.Password);

			if (user == null)
			{
				Response.StatusCode = 400;
				return Request.HttpContext.Response.WriteAsync(ApplicationError.GetErrorMessage(ErrorCode.TC0009));
			}
			parameters.Username = user.Email;
			parameters.UserId = user.MemberID.ToString();
			var refresh_token = Guid.NewGuid().ToString().Replace("-", "");

			var rToken = new Rtoken
			{
				RefreshToken = refresh_token,
				//Id = Guid.NewGuid(),
				IsStop = 0,
				CreatedTimestamp = DateTime.Now
			};

			//store the refresh_token   
			if (_tokenService.AddToken(rToken))
			{
				return GetJwt(refresh_token, parameters.Username, parameters.UserId, user.Role);
			}
			else
			{
				Response.StatusCode = 400;
				return Request.HttpContext.Response.WriteAsync(ApplicationError.GetErrorMessage(ErrorCode.TC0010));
			}
		}

		//scenario 2 ï¼š get the access_token by refresh_token  
		private Task DoRefreshToken(Parameters parameters)
		{
			var token = _tokenService.GetToken(parameters.Refresh_token);

			if (token == null)
			{
				Response.StatusCode = 400;
				return Request.HttpContext.Response.WriteAsync(ApplicationError.GetErrorMessage(ErrorCode.TC0011));
			}

			if (token.IsStop == 1)
			{
				Response.StatusCode = 400;
				return Request.HttpContext.Response.WriteAsync(ApplicationError.GetErrorMessage(ErrorCode.TC0012));
			}

			var refresh_token = Guid.NewGuid().ToString().Replace("-", "");

			token.IsStop = 1;
			//expire the old refresh_token and add a new refresh_token  
			var updateFlag = _tokenService.ExpireToken(token);

			var addFlag = _tokenService.AddToken(new Rtoken
			{
				RefreshToken = refresh_token,
				//Id = Guid.NewGuid(),
				IsStop = 0,
				CreatedTimestamp = DateTime.Now
			});

			if (updateFlag && addFlag)
			{
				return GetJwt(refresh_token);
			}
			else
			{
				Response.StatusCode = 400;
				return Request.HttpContext.Response.WriteAsync(ApplicationError.GetErrorMessage(ErrorCode.TC0013));
			}
		}


		private async Task GetJwt(string refresh_token, string username, string userId, string role)
		{
			var now = DateTime.UtcNow;
			var claims = new Claim[]
			{
			new Claim(JwtRegisteredClaimNames.Sub, username),
			new Claim(JwtRegisteredClaimNames.Email, username),
			new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
			new Claim(JwtRegisteredClaimNames.Iat, now.ToUniversalTime().ToString(), ClaimValueTypes.Integer64),
				new Claim("UserId",userId),
				new Claim("Role", role),
				 new Claim(ClaimTypes.Role, role)
			};

			var symmetricKeyAsBase64 = ConfigurationManager.AppSettings["SecretKey"];
			var keyByteArray = Encoding.ASCII.GetBytes(symmetricKeyAsBase64);
			var signingKey = new SymmetricSecurityKey(keyByteArray);

			var jwt = new JwtSecurityToken(
				issuer: ConfigurationManager.AppSettings["Issuer"],
				audience: ConfigurationManager.AppSettings["Audience"],
				claims: claims,
				notBefore: now,
				expires: now.Add(TimeSpan.FromHours(24)),
				signingCredentials: new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256));
			var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

			var response = new
			{
				access_token = encodedJwt,
				username,
				expires_in = (int)TimeSpan.FromHours(24).TotalSeconds,
				refresh_token,
				role
			};
			Request.HttpContext.Response.ContentType = "application/json";
			await Request.HttpContext.Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
		}

		private async Task GetJwt(string refresh_token)
		{
			var now = DateTime.UtcNow;

			var claims = new Claim[]
			{
			new Claim(JwtRegisteredClaimNames.Sub, refresh_token),
			new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
			new Claim(JwtRegisteredClaimNames.Iat, now.ToUniversalTime().ToString(), ClaimValueTypes.Integer64)
			};

			var symmetricKeyAsBase64 = ConfigurationManager.AppSettings["SecretKey"];
			var keyByteArray = Encoding.ASCII.GetBytes(symmetricKeyAsBase64);
			var signingKey = new SymmetricSecurityKey(keyByteArray);

			var jwt = new JwtSecurityToken(
				issuer: ConfigurationManager.AppSettings["Issuer"],
				audience: ConfigurationManager.AppSettings["Audience"],
				claims: claims,
				notBefore: now,
				expires: now.Add(TimeSpan.FromHours(24)),
				signingCredentials: new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256));
			var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

			var response = new
			{
				access_token = encodedJwt,
				expires_in = (int)TimeSpan.FromHours(24).TotalSeconds,
				refresh_token
			};

			Request.HttpContext.Response.ContentType = "application/json";
			await Request.HttpContext.Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
		}
	}
}