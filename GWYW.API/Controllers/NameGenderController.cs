﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GWYW.Model;
using GWYW.Service.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GWYW.API.Controllers
{
	[Authorize]
	[Route("api/[controller]")]
    [ApiController]
    public class NameGenderController : ControllerBase
    {
		private INameGenderService _nameGenderService;

		public NameGenderController(INameGenderService nameGenderService)
		{
			_nameGenderService = nameGenderService;
		}


		/// <summary>
		/// Get all Shops detail
		/// </summary>GWYWmemberfaves
		/// <returns></returns>
		[HttpGet("GetAllNameGender")]
		public async Task<IActionResult> GetAllNameGender(int pageSize = 10, int pageIndex = 0)
		{
			return Ok(await _nameGenderService.GetAllNameGenderDetails(pageSize, pageIndex));
		}


		/// <summary>
		/// Get Member By Id
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		[HttpGet]
		[Route("GetNameGender")]
		[ProducesResponseType(StatusCodes.Status200OK)]
		[ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
		[ProducesDefaultResponseType]
		public async Task<IActionResult> GetNameGender(int? id)
		{
			if (id != null)
			{
				return Ok(await _nameGenderService.GetNamegenderDetail(id));
			}
			else
			{
				return BadRequest(/*ExtractValidationErrors()*/);
			}
		}

		/// <summary>
		/// Create Memberfaves 
		/// </summary>
		/// <remarks>
		/// Sample request for create Member
		/// 
		///     POST /api/Members/CreateMember
		///     {
		///         "title":"string", [required]
		///     }
		/// </remarks>
		/// <param name="model"></param>
		/// <param name="file"></param>
		/// <returns></returns>
		[HttpPost]
		[Route("CreateNameGender")]
		[ProducesResponseType(StatusCodes.Status200OK)]
		[ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
		[ProducesDefaultResponseType]
		public async Task<IActionResult> CreateNameGender([FromBody]GWYWnamegenderModel model)
		{
			if (ModelState.IsValid)
			{
				return Ok(await _nameGenderService.CreateNewNameGender(model));
			}
			else
			{
				return BadRequest(/*ExtractValidationErrors()*/);
			}
		}


		/// <summary>
		/// Update Member profile (api update only first name , lastname and profile picture)
		/// </summary>
		/// <remarks>
		/// Sample request for update admin profile
		/// </remarks>
		/// <returns></returns>
		[HttpPut]
		[Route("UpdateNameGender")]
		public async Task<IActionResult> UpdateNameGender([FromBody]GWYWnamegenderModel model)
		{
			if (ModelState.IsValid)
			{
				return Ok(await _nameGenderService.UpdateNameGenderProfile(model));
			}
			else
			{
				return BadRequest(/*ExtractValidationErrors()*/);
			}	
		}

		[HttpDelete]
		[Route("DeleteNameGender")]
		public async Task<IActionResult> DeleteNameGender(int id)
		{
			return Ok(await _nameGenderService.DeleteNameGenderProfile(id));
		}
	}
}