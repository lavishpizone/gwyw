﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using GWYW.Model;
using GWYW.Service.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GWYW.API.Controllers
{
	[Authorize]
	[Route("api/[controller]")]
    [ApiController]
    public class MembersController : ControllerBase
    {
		private IMemberService _memberService;

		public MembersController(IMemberService memberService)
		{
			_memberService = memberService;
		}

		/// <summary>
		/// Get all members detail
		/// </summary>
		/// <returns></returns>
		[HttpGet("GetAllMembers")]
		public async Task<IActionResult> GetAllMembers(int pageSize = 10, int pageIndex = 0)
		{
			return Ok(await _memberService.GetAllMembersDetails(pageSize, pageIndex));
		}

		/// <summary>
		/// Get Member By Id
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		[HttpGet]
		[Route("GetMember")]
		[ProducesResponseType(StatusCodes.Status200OK)]
		[ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
		[ProducesDefaultResponseType]
		public async Task<IActionResult> GetMember(int? id)
		{
			if (id != null)
			{
				return Ok(await _memberService.GetMember(id));
			}
			else
			{
				return BadRequest(/*ExtractValidationErrors()*/);
			}
		}

		/// <summary>
		/// Create Member 
		/// </summary>
		/// <remarks>
		/// Sample request for create Member
		/// 
		///     POST /api/Members/CreateMember
		///     {
		///         "title":"string", [required]
		///			FirstName = [required]
		///			LastName = [required]
		///			DOB = [required]
		///			Gender = [required]
		///			StreetAddress1 =[required]
		///			City =[required]
		///			StateCode [required]
		///			Zip = Zip,[required]
		///			Email =.Email,[required]
		///			Password = Instance.Encrypt(s.Password),[required]
		///			Role = Role,[required]
		///     }
		///     
		/// </remarks>
		/// <param name="model"></param>
		/// <param name="file"></param>
		/// <returns></returns>
		[HttpPost]
		[Route("InsertMember")]
		[ProducesResponseType(StatusCodes.Status200OK)]
		[ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
		[ProducesDefaultResponseType]		
		public async Task<IActionResult> InsertMember([FromBody]GWYWmembersModel membersModel)
		{
			//ViewData[Email] = membersModel.Email;
			return Ok(await _memberService.CreateMember(membersModel));
		}
		
		//	if (ModelState.IsValid)
		//	{
		//		string baseURL = $"{Request.Scheme}://{Request.Host}";
		//		return Ok(await _memberService.CreateMember(model, baseURL));
		//	}
		//	else
		//	{
		//		return BadRequest(/*ExtractValidationErrors()*/);
		//	}
	


		/// <summary>
		/// Update Member profile (api update only first name , lastname and profile picture)
		/// </summary>
		/// <remarks>
		/// Sample request for update admin profile
		/// </remarks>
		/// <param name="userModel"></param>
		/// <returns></returns>
		[HttpPut]
		[Route("UpdateMember")]
		public async Task<IActionResult> UpdateMember([FromBody]GWYWmembersModel model)
		{
			return Ok(await _memberService.UpdateProfile(model));
		}


		[HttpDelete]
		[Route("DeleteMember")]
		public async Task<IActionResult> DeleteMember(int id)
		{
			return Ok(await _memberService.DeleteProfile(id));
		}


		[HttpPost]
		[Route("ForgotPassword")]
		public async Task<IActionResult> ForgotPassword(string Email)
		{
			return Ok(await _memberService.PasswordForgot(Email));
		}

		[HttpPost]
		[Route("ResetPassword")]
		public async Task<IActionResult> ResetPassword([FromBody]ResetpasswordModel resetpasswordModel)
		{
			return Ok(await _memberService.PasswordReset(resetpasswordModel));
		}


		/// <summary>
		/// Get all members detail
		/// </summary>GWYWmemberfaves
		/// <returns></returns>
		[HttpGet("GetAllMemberFaves")]
		public async Task<IActionResult> GetAllMemberFaves(int pageSize = 10, int pageIndex = 0)
		{
			return Ok(await _memberService.GetAllMembersFavesDetails(pageSize, pageIndex));
		}


		/// <summary>
		/// Get Member By Id
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		[HttpGet]
		[Route("GetMemberFaves")]
		[ProducesResponseType(StatusCodes.Status200OK)]
		[ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
		[ProducesDefaultResponseType]
		public async Task<IActionResult> GetMemberFaves(int? id)
		{
			if (id != null)
			{
				return Ok(await _memberService.GetMemberFaves(id));
			}
			else
			{
				return BadRequest(/*ExtractValidationErrors()*/);
			}
		}


		/// <summary>
		/// Create Memberfaves 
		/// </summary>
		/// <remarks>
		/// Sample request for create Member
		/// 
		///     POST /api/Members/CreateMember
		///     {
		///         "title":"string", [required]
		///			
		///     }
		///     
		/// </remarks>
		/// <param name="model"></param>
		/// <param name="file"></param>
		/// <returns></returns>
		[HttpPost]
		[Route("CreateMemberFaves")]
		[ProducesResponseType(StatusCodes.Status200OK)]
		[ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
		[ProducesDefaultResponseType]
		public async Task<IActionResult> CreateMember([FromBody]GWYWmemberfavesModel model)
		{
			if (ModelState.IsValid)
			{
				//string baseURL = $"{Request.Scheme}://{Request.Host}";
				return Ok(await _memberService.CreateMemberFave(model));
			}
			else
			{
				return BadRequest(/*ExtractValidationErrors()*/);
			}
		}


		/// <summary>
		/// Update Member profile (api update only first name , lastname and profile picture)
		/// </summary>
		/// <remarks>
		/// Sample request for update admin profile
		/// </remarks>
		/// <param name="userModel"></param>
		/// <returns></returns>
		[HttpPut]
		[Route("UpdateMemberFaves")]
		public async Task<IActionResult> UpdateMemberFaves([FromBody]GWYWmemberfavesModel model)
		{
			return Ok(await _memberService.UpdateMemberFavesProfile(model));
		}


		[HttpDelete]
		[Route("DeleteMemberFaves")]
		public async Task<IActionResult> DeleteMemberFaves(int id)
		{
			return Ok(await _memberService.DeleteMemberFavesProfile(id));
		}




	}
}