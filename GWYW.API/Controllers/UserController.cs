﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GWYW.Model;
using GWYW.Service.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GWYW.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
		private IUserService _userService;
		
		public UserController(IUserService userService)
		{
			_userService = userService;
		}

		[HttpPost]
		[Route("GeneralSetting")]
		public async Task<IActionResult> GeneralSetting([FromBody]GeneralSettingModel generalSetting)
		{
			return Ok(await _userService.Setting(generalSetting));
		}
	}
}