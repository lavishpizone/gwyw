﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GWYW.Model;
using GWYW.Service.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GWYW.API.Controllers
{
	[Authorize]
	[Route("api/[controller]")]
    [ApiController]
    public class ShopController : ControllerBase
    {
		private IShopService _shopService;

		public ShopController(IShopService shopService)
		{
			_shopService = shopService;
		}


		/// <summary>
		/// Get all Shops detail
		/// </summary>GWYWmemberfaves
		/// <returns></returns>
		[HttpGet("GetAllShops")]
		public async Task<IActionResult> GetAllShops(int pageSize = 10, int pageIndex = 0)
		{
			return Ok(await _shopService.GetAllShopsDetails(pageSize, pageIndex));
		}


		/// <summary>
		/// Get Member By Id
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		[HttpGet]
		[Route("GetShop")]
		[ProducesResponseType(StatusCodes.Status200OK)]
		[ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
		[ProducesDefaultResponseType]
		public async Task<IActionResult> GetShop(int? id)
		{
			if (id != null)
			{
				return Ok(await _shopService.GetShopDetail(id));
			}
			else
			{
				return BadRequest(/*ExtractValidationErrors()*/);
			}
		}

		/// <summary>
		/// Create Memberfaves 
		/// </summary>
		/// <remarks>
		/// Sample request for create Member
		/// 
		///     POST /api/Members/CreateMember
		///     {
		///         "title":"string", [required]
		///     }
		/// </remarks>
		/// <param name="model"></param>
		/// <param name="file"></param>
		/// <returns></returns>
		[HttpPost]
		[Route("CreateShop")]
		[ProducesResponseType(StatusCodes.Status200OK)]
		[ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
		[ProducesDefaultResponseType]
		public async Task<IActionResult> CreateShop([FromBody]GWYWshopsModel model)
		{
			if (ModelState.IsValid)
			{
				return Ok(await _shopService.CreateNewShop(model));
			}
			else
			{
				return BadRequest(/*ExtractValidationErrors()*/);
			}
		}

		/// <summary>
		/// Update Member profile ()
		/// </summary>
		/// <remarks>
		/// Sample request for update admin profile
		/// </remarks>
		/// <param name=""></param>
		/// <returns></returns>
		[HttpPut]
		[Route("UpdateShop")]
		public async Task<IActionResult> UpdateShop([FromBody]GWYWshopsModel model)
		{
			if (ModelState.IsValid)
			{
				return Ok(await _shopService.UpdateShopProfile(model));
			}
			else
			{
				return BadRequest(/*ExtractValidationErrors()*/);
			}

		}

		[HttpDelete]
		[Route("DeleteShop")]
		public async Task<IActionResult> DeleteMemberFaves(int id)
		{
			return Ok(await _shopService.DeleteShopProfile(id));
		}


	}
}