﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GWYW.API.Filters;
using GWYW.Entity;
using GWYW.Service.Interface;
using GWYW.Service.Service;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Swashbuckle.AspNetCore.Swagger;

namespace GWYW.API
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddMvc(options =>
			{
				options.Filters.Add(new ApiExceptionFilter());

			}).AddJsonOptions(options =>
			{
				options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
				options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
			});
			//services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

			services.AddDbContext<GWYWcontext>(options =>
				options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

			// configure DI for application services
			services.AddTransient<IUnitOfWork, UnitOfWork<GWYWcontext>>();
			services.AddTransient<IEmailSenderService, EmailSenderService>();
			services.AddScoped<ITokenService, TokenService>();
			services.AddScoped<IUserService, UserService>();
			services.AddScoped<IMemberService, MemberService>();
			services.AddScoped<IShopService, ShopService>();
			services.AddScoped<INameGenderService, NameGenderService>();

			services.AddCors(options =>
			{
				options.AddPolicy("CorsPolicy",
					builder => builder
						  //.WithOrigins(Configuration["SignalR:UIOrigin"])
						  .AllowAnyOrigin()
						  .AllowAnyMethod()
						  .AllowAnyHeader()
						  .AllowCredentials()
					.Build());
			});

			ConfigureJwtAuthService(services);

			services.AddSwaggerGen(c =>
			{
				c.SwaggerDoc("v1", new Info { Title = "GWYW API", Version = "v1" });
				//c.SwaggerDoc("v1", new Info
				//{
				//    Version = "v1",
				//    Title = "ToDo API",
				//    Description = "A simple example ASP.NET Core Web API",
				//    TermsOfService = "None",
				//    Contact = new Contact
				//    {
				//        Name = "Shayne Boyer",
				//        Email = string.Empty,
				//        Url = "https://twitter.com/spboyer"
				//    },
				//    License = new License
				//    {
				//        Name = "Use under LICX",
				//        Url = "https://example.com/license"
				//    }
				//});

				c.AddSecurityDefinition("Bearer", new ApiKeyScheme { In = "header", Description = "Please enter JWT with Bearer into field", Name = "Authorization", Type = "apiKey" });
				c.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>> {
				{ "Bearer", Enumerable.Empty<string>() },
				});


				//// Set the comments path for the Swagger JSON and UI.
				//var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
				//var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
				//c.IncludeXmlComments(xmlPath);
			});

		}

		private static void UpdateDatabase(IApplicationBuilder app)
		{
			using (var serviceScope = app.ApplicationServices
				.GetRequiredService<IServiceScopeFactory>()
				.CreateScope())
			{
				using (var context = serviceScope.ServiceProvider.GetService<GWYWcontext>())
				{
					context.Database.Migrate();
				}
			}
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
		{
			loggerFactory.AddConsole(Configuration.GetSection("Logging")); //log levels set in your configuration
			loggerFactory.AddFile("Logs/GWYW-{Date}.txt");
			loggerFactory.AddDebug(); //does all log levels

			UpdateDatabase(app);
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			else
			{
				// The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
				app.UseHsts();
			}

			// Enable middleware to serve generated Swagger as a JSON endpoint.
			app.UseSwagger();

			// Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), 
			// specifying the Swagger JSON endpoint.
			app.UseSwaggerUI(c =>
			{
				c.SwaggerEndpoint("/swagger/v1/swagger.json", "GWYW API V1");
				c.RoutePrefix = string.Empty;
			});

			app.UseAuthentication();
			app.UseCors("CorsPolicy");
			app.UseMvc();
			app.UseHttpsRedirection();
			app.UseMvc();
		}

		public void ConfigureJwtAuthService(IServiceCollection services)
		{
			var symmetricKeyAsBase64 = ConfigurationManager.AppSettings["SecretKey"];
			var keyByteArray = Encoding.ASCII.GetBytes(symmetricKeyAsBase64);
			var signingKey = new SymmetricSecurityKey(keyByteArray);

			var tokenValidationParameters = new TokenValidationParameters
			{
				// The signing key must match!
				ValidateIssuerSigningKey = true,
				IssuerSigningKey = signingKey,

				// Validate the JWT Issuer (iss) claim
				ValidateIssuer = true,
				ValidIssuer = ConfigurationManager.AppSettings["Issuer"],

				// Validate the JWT Audience (aud) claim
				ValidateAudience = true,
				ValidAudience = ConfigurationManager.AppSettings["Audience"],

				// Validate the token expiry
				ValidateLifetime = true,

				ClockSkew = TimeSpan.Zero
			};

			services.AddAuthentication(options =>
			{
				options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
				options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
			})
			.AddJwtBearer(o =>
			{
				o.TokenValidationParameters = tokenValidationParameters;
				o.Events = new JwtBearerEvents
				{
					OnMessageReceived = context =>
					{
						if (context.Request.Path.Value.StartsWith("/notify") &&
							context.Request.Query.TryGetValue("token", out StringValues token)
						)
						{
							context.Token = token;
							context.Options.Validate();
						}

						return Task.CompletedTask;
					},
					OnAuthenticationFailed = context =>
					{
						var te = context.Exception;
						return Task.CompletedTask;
					}
				};
			});
		}
	}
}
