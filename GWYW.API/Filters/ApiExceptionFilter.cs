﻿using GWYW.Service.Exception;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace GWYW.API.Filters
{
	public class ApiExceptionFilter : IExceptionFilter
	{
		public void OnException(ExceptionContext context)
		{
			HttpStatusCode status;
			String message;
			context.ExceptionHandled = true;
			var exceptionType = context.Exception.GetType();

			if (exceptionType == typeof(UnauthorizedAccessException))
			{
				message = "Unauthorized Access";
				status = HttpStatusCode.Unauthorized;
			}
			else if (exceptionType == typeof(NotImplementedException))
			{
				message = "A server error occurred.";
				status = HttpStatusCode.NotImplemented;
			}
			else if (exceptionType == typeof(GWYWServiceException))
			{
				message = context.Exception.Message;// +" : "+context.Exception?.InnerException?.Message;
				status = HttpStatusCode.BadRequest;
			}
			else if (exceptionType == typeof(EmergeIQUnAuthorizedException))
			{
				message = context.Exception.Message;// +"InnerException"+context.Exception?.InnerException?.ToString();
				status = HttpStatusCode.Unauthorized;
			}
			else
			{
				message = context.Exception.Message;
				status = HttpStatusCode.NotFound;
			}

			context.HttpContext.Response.StatusCode = (int)status;
			context.HttpContext.Response.ContentType = "application/json";

			var err = message;// + " " + context.Exception.StackTrace;
			context.HttpContext.Response.Body.WriteAsync(Encoding.ASCII.GetBytes(err), 0, err.Length).ConfigureAwait(false);
		}
	}

	public class EmergeIQUnAuthorizedException : Exception
	{
		/// <summary>
		/// 
		/// </summary>
		public EmergeIQUnAuthorizedException()
		{ }
		/// <summary>
		/// 
		/// </summary>
		/// <param name="message"></param>
		public EmergeIQUnAuthorizedException(string message)
			: base(message)
		{ }
		/// <summary>
		/// 
		/// </summary>
		/// <param name="message"></param>
		/// <param name="innerException"></param>
		public EmergeIQUnAuthorizedException(string message, Exception innerException)
			: base(message, innerException)
		{ }
	}
}
