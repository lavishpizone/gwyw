﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace GWYW.Model
{
	public class ResetpasswordModel
	{
		public int MemberID { get; set; }
		public string Email { get; set; }
		[Required]
		public string OldPassword { get; set; }
		[Required]
		public string NewPassword { get; set; }
		[Required]
		public string ConfirmPassword { get; set; }
	}
}
