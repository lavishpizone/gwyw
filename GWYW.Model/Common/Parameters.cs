﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace GWYW.Model.Common
{
	public class Parameters
	{
		public string Grant_type { get; set; }

		public string Refresh_token { get; set; }

		public string UserId { get; set; }

		[MaxLength(50)]
		[Required(ErrorMessage = "Username required", AllowEmptyStrings = false)]
		[EmailAddress]
		[Display(Name = "Username")]
		public string Username { get; set; }

		[Required(ErrorMessage = "Password required", AllowEmptyStrings = false)]
		[DataType(System.ComponentModel.DataAnnotations.DataType.Password)]
		[StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
		[Display(Name = "Password")]
		public string Password { get; set; }

		public string Token { get; set; }
	}
}
