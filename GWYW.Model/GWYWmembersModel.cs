﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace GWYW.Model
{
	public class GWYWmembersModel
	{
		public int MemberID { get; set; }
		[Required(ErrorMessage = "Required")]
		[StringLength(50, MinimumLength = 3, ErrorMessage = "Minimum 3 characters required")]
		public string FirstName { get; set; }
		[Required(ErrorMessage = "Required")]
		[StringLength(50, MinimumLength = 3, ErrorMessage = "Minimum 3 characters required")]
		public string LastName { get; set; }
		[Required(ErrorMessage = "Required")]
		[StringLength(50, MinimumLength = 3, ErrorMessage = "Email Required")]
		public string Email { get; set; }
		[Required(ErrorMessage = "Required")]
		[StringLength(10, MinimumLength = 10, ErrorMessage = "10 Disgits required")]
		public string CellNumber { get; set; }
		[Required]
		public DateTime DOB { get; set; }
		[Required(ErrorMessage = "Required")]
		[StringLength(1, MinimumLength = 1, ErrorMessage = "One character required")]
		public string Gender { get; set; }
		[Required(ErrorMessage = "Required")]
		[StringLength(50, MinimumLength = 3, ErrorMessage = "Minimum 3 characters required")]
		public string Password { get; set; }
		[Required(ErrorMessage = "Required")]
		[StringLength(50, MinimumLength = 3, ErrorMessage = "Required")]
		public string StreetAddress1 { get; set; }
		public string StreetAddress2 { get; set; }
		[Required(ErrorMessage = "Required")]
		[StringLength(50, MinimumLength = 2, ErrorMessage = "Required")]
		public string City { get; set; }
		[Required(ErrorMessage = "Required")]
		[StringLength(2, MinimumLength = 1, ErrorMessage = "2 digit code required")]
		public string StateCode { get; set; }
		[Required(ErrorMessage = "Required")]
		[StringLength(10, MinimumLength = 1, ErrorMessage = "Role required")]
		public string Role { get; set; }
		[Required(ErrorMessage = "Required")]
		[StringLength(5, MinimumLength = 5, ErrorMessage = "5 digit code required")]
		public string Zip { get; set; }
		public string ZipPlus5 { get; set; }
		public DateTime CreationDate { get; set; }
	}
}
