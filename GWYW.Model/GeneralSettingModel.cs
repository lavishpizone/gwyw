﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace GWYW.Model
{
	public class GeneralSettingModel
	{
		public int ID { get; set; }
		[Required]
		public string Key { get; set; }
		public string Value { get; set; }
	}
}
