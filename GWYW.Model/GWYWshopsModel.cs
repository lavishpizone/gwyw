﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace GWYW.Model
{
	public class GWYWshopsModel
	{
		public int ShopID { get; set; }
		[Required(ErrorMessage = "Required")]
		[StringLength(50, MinimumLength = 3, ErrorMessage = "Minimum 3 characters required")]
		public string ShopName { get; set; }
		[Required(ErrorMessage = "Required")]
		[StringLength(1, MinimumLength = 1, ErrorMessage = "One character only")]
		public string BMorONLINE { get; set; }
		[Required(ErrorMessage = "Required")]
		[StringLength(1, MinimumLength = 1, ErrorMessage = "One character only")]
		public string ShopType { get; set; }
	}
}
