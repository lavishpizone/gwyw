﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace GWYW.Model
{
	public class GWYWnamegenderModel
	{
		public int NameID { get; set; }
		[Required(ErrorMessage = "Required")]
		[StringLength(50, MinimumLength = 3, ErrorMessage = "Minimum 3 characters required")]
		public string NameValue { get; set; }
		[Required(ErrorMessage = "Required")]
		[StringLength(1, MinimumLength = 1, ErrorMessage = "One character only")]
		public string Gender { get; set; }
	}
}
