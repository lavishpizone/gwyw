﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace GWYW.Entity
{
	public class GWYWshops
	{
		public GWYWshops()
		{
			GWYWshopIdNavigation = new HashSet<GWYWmemberfaves>();
		}

		[Key]
		public int ShopID { get; set; }
		[Required]
		[Column(TypeName = "VARCHAR(50)")]
		//[MaxLength(50)]
		public string ShopName { get; set; }
		[Required]
		[Column(TypeName = "CHAR(1)")]
		//[MaxLength(1)]
		public string BMorONLINE { get; set; }
		[Required]
		[Column(TypeName = "CHAR(1)")]
		//[MaxLength(1)]
		public string ShopType { get; set; }

		public ICollection<GWYWmemberfaves> GWYWshopIdNavigation { get; set; }
	}
}
