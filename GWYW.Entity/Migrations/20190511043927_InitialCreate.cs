﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace GWYW.Entity.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "GeneralSettings",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Key = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GeneralSettings", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "GWYWmembers",
                columns: table => new
                {
                    MemberID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FirstName = table.Column<string>(type: "VARCHAR(50)", nullable: false),
                    LastName = table.Column<string>(type: "VARCHAR(50)", nullable: false),
                    CellNumber = table.Column<string>(type: "CHAR(10)", nullable: false),
                    Email = table.Column<string>(type: "VARCHAR(50)", nullable: false),
                    DOB = table.Column<DateTime>(type: "DateTime", nullable: false),
                    Gender = table.Column<string>(type: "CHAR(1)", nullable: false),
                    Password = table.Column<string>(type: "VARCHAR(50)", nullable: false),
                    StreetAddress1 = table.Column<string>(type: "VARCHAR(50)", nullable: false),
                    StreetAddress2 = table.Column<string>(type: "VARCHAR(50)", nullable: true),
                    City = table.Column<string>(type: "VARCHAR(50)", nullable: false),
                    StateCode = table.Column<string>(type: "CHAR(2)", nullable: false),
                    Role = table.Column<string>(nullable: false),
                    Zip = table.Column<string>(type: "CHAR(5)", nullable: false),
                    ZipPlus5 = table.Column<string>(type: "CHAR(5)", nullable: true),
                    CreationDate = table.Column<DateTime>(type: "DateTime", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GWYWmembers", x => x.MemberID);
                });

            migrationBuilder.CreateTable(
                name: "GWYWnamegender",
                columns: table => new
                {
                    NameID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    NameValue = table.Column<string>(type: "VARCHAR(50)", nullable: false),
                    Gender = table.Column<string>(type: "CHAR(1)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GWYWnamegender", x => x.NameID);
                });

            migrationBuilder.CreateTable(
                name: "GWYWshops",
                columns: table => new
                {
                    ShopID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ShopName = table.Column<string>(type: "VARCHAR(50)", nullable: false),
                    BMorONLINE = table.Column<string>(type: "CHAR(1)", nullable: false),
                    ShopType = table.Column<string>(type: "CHAR(1)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GWYWshops", x => x.ShopID);
                });

            migrationBuilder.CreateTable(
                name: "Rtoken",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RefreshToken = table.Column<string>(nullable: false),
                    IsStop = table.Column<int>(nullable: false),
                    CreatedTimestamp = table.Column<DateTime>(type: "DateTime", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Rtoken", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "GWYWmemberfaves",
                columns: table => new
                {
                    MemberFaveID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    MemberID = table.Column<int>(nullable: false),
                    ShopID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GWYWmemberfaves", x => x.MemberFaveID);
                    table.ForeignKey(
                        name: "FK_GWYWmemberfaves_GWYWmembers_MemberID",
                        column: x => x.MemberID,
                        principalTable: "GWYWmembers",
                        principalColumn: "MemberID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_GWYWmemberfaves_GWYWshops_ShopID",
                        column: x => x.ShopID,
                        principalTable: "GWYWshops",
                        principalColumn: "ShopID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_GWYWmemberfaves_MemberID",
                table: "GWYWmemberfaves",
                column: "MemberID");

            migrationBuilder.CreateIndex(
                name: "IX_GWYWmemberfaves_ShopID",
                table: "GWYWmemberfaves",
                column: "ShopID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GeneralSettings");

            migrationBuilder.DropTable(
                name: "GWYWmemberfaves");

            migrationBuilder.DropTable(
                name: "GWYWnamegender");

            migrationBuilder.DropTable(
                name: "Rtoken");

            migrationBuilder.DropTable(
                name: "GWYWmembers");

            migrationBuilder.DropTable(
                name: "GWYWshops");
        }
    }
}
