﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace GWYW.Entity
{
	public class GWYWmemberfaves
	{
		[Key]
		public int MemberFaveID { get; set; }
		
		public int MemberID { get; set; }

		public int ShopID { get; set; }

		[ForeignKey("MemberID")]
		public GWYWmembers MemberIDNavigation { get; set; }

		[ForeignKey("ShopID")]
		public GWYWshops ShopIDNavigation { get; set; }

	}
}
