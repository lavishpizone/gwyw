﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace GWYW.Entity
{
	public class Rtoken
	{
		[Key]
		public int Id { get; set; }
		[Required]
		public string RefreshToken { get; set; }
		[Required]
		public int IsStop { get; set; }
		[Required]
		[Column("CreatedTimestamp", TypeName = "DateTime")]
		public DateTime CreatedTimestamp { get; set; }
	}
}
