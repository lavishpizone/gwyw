﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace GWYW.Entity
{
	public class GWYWcontext : DbContext
	{
		public GWYWcontext(DbContextOptions<GWYWcontext> options)
			: base(options)
		{
		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);
			// Customize the ASP.NET Identity model and override the defaults if needed.
			// For example, you can rename the ASP.NET Identity table names and more.
			// Add your customizations after calling base.OnModelCreating(builder);


			modelBuilder.Entity<GWYWmemberfaves>(e =>
			{
				e.HasOne(x => x.MemberIDNavigation)
			   .WithMany(m => m.GWYWmemberIdNavigation)
			   .HasForeignKey(x => x.MemberID)
			   .OnDelete(DeleteBehavior.ClientSetNull);
			});

			modelBuilder.Entity<GWYWmemberfaves>(e =>
			{
				e.HasOne(x => x.ShopIDNavigation)
			   .WithMany(m => m.GWYWshopIdNavigation)
			   .HasForeignKey(x => x.ShopID)
			   .OnDelete(DeleteBehavior.ClientSetNull);
			});
					   
		}

		public DbSet<GWYWmembers> GWYWmembers { get; set; }

		public DbSet<GWYWmemberfaves> GWYWmemberfaves { get; set; }

		public DbSet<GWYWnamegender> GWYWnamegender { get; set; }

		public DbSet<GWYWshops> GWYWshops { get; set; }

		public DbSet<Rtoken> Rtoken { get; set; }

		public DbSet<GeneralSetting> GeneralSettings { get; set; }

	}
}
