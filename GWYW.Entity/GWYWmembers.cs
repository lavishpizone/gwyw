﻿	using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace GWYW.Entity
{
	public class GWYWmembers
	{
		public GWYWmembers()
		{
			GWYWmemberIdNavigation = new HashSet<GWYWmemberfaves>();
		}

		[Key]
		public int MemberID { get; set; }
		[Required]
		[Column(TypeName = "VARCHAR(50)")]
		//[MaxLength(50)]
		public string FirstName { get; set; }
		[Required]
		[Column(TypeName = "VARCHAR(50)")]
		//[MaxLength(50)]
		public string LastName { get; set; }
		[Required]
		[Column(TypeName = "CHAR(10)")]
		//[MaxLength(10)]
		public string CellNumber { get; set; }
		[Required]
		[Column(TypeName = "VARCHAR(50)")]
		//[MaxLength(50)]
		public string Email { get; set; }
		[Required]
		[Column("DOB", TypeName = "DateTime")]
		public DateTime DOB { get; set; }
		[Required]
		[Column(TypeName = "CHAR(1)")]
		//[MaxLength(1)]
		public string Gender { get; set; }
		[Required]
		[Column(TypeName = "VARCHAR(50)")]
		//[MaxLength(50)]
		public string Password { get; set; }
		[Required]
		[Column(TypeName = "VARCHAR(50)")]
		//[MaxLength(50)]
		public string StreetAddress1 { get; set; }
		[Column(TypeName = "VARCHAR(50)")]
		//[MaxLength(50)]
		public string StreetAddress2 { get; set; }
		[Required]
		[Column(TypeName = "VARCHAR(50)")]
		//[MaxLength(50)]
		public string City { get; set; }
		[Required]
		[Column(TypeName = "CHAR(2)")]
		public string StateCode { get; set; }
		[Required]
		public string Role { get; set; }
		[Required]
		[Column(TypeName = "CHAR(5)")]
		public string Zip { get; set; }
		[Column(TypeName = "CHAR(5)")]
		public string ZipPlus5 { get; set; }
		[Column("CreationDate", TypeName = "DateTime")]
		public DateTime CreationDate { get; set; }

		public ICollection<GWYWmemberfaves> GWYWmemberIdNavigation { get; set; }
	}
}
