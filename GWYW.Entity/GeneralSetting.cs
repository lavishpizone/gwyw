﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace GWYW.Entity
{
	public class GeneralSetting
	{
		[Key]
		public int ID { get; set; }
		[Required]
		public string Key { get; set; }
		public string Value { get; set; }
	}
}
