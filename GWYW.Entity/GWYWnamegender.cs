﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace GWYW.Entity
{
	public class GWYWnamegender
	{
		[Key]
		public int NameID { get; set; }
		[Required]
		[Column(TypeName = "VARCHAR(50)")]
		public string NameValue { get; set; }
		[Column(TypeName = "CHAR(1)")]
		public string Gender { get; set; }
	}
}
